Ingredient {{ $ingredient->name }} is running low. Please order more supplies.

Current quantity: {{ $ingredient->stock_quantity }}
