<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Ingredient;
use App\Models\Product;
use App\Models\ProductIngredient;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->call(IngredientSeeder::class);

        $products = [
            'Sandwich' => [
                'Lettuce' => 1,
                'Bacon' => 2,
                'Bread' => 2,
            ],
            'Salad' => [
                'Tofu' => 1,
                'Lettuce' => 2,
                'Tomato' => 3,
            ],
            'Ramen' => [
                'Egg' => 1,
                'Noodles' => 100,
                'Chicken' => 2,
                'Broth' => 1,
            ]
        ];

        $ingredients = Ingredient::all()->mapWithKeys(static fn (Ingredient $item) => [$item->name => $item ]);

        DB::beginTransaction();
        foreach ($products as $productName => $productIngredients) {
            $product = new Product();
            $product->name = $productName;

            $product->save();

            foreach ($productIngredients as $ingredientName => $quantity) {
                $productIngredient = new ProductIngredient();
                $productIngredient->ingredient()->associate($ingredients[$ingredientName]);
                $productIngredient->product()->associate($product);
                $productIngredient->quantity = $quantity;

                $productIngredient->save();
            }
        }
        DB::commit();
    }
}
