<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Ingredient;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IngredientSeeder extends Seeder
{
    public const ingredients = [
        'Lettuce',
        'Tomato',
        'Tofu',
        'Bacon',
        'Chicken',
        'Ketchup',
        'Bread',
        'Egg',
        'Noodles',
        'Broth',
    ];


    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::beginTransaction();
        foreach (static::ingredients as $ingredientName) {
            $ingredient = new Ingredient();
            $ingredient->name = $ingredientName;
            $ingredient->stock_quantity = 0;
            $ingredient->warning_level = 500;
            $ingredient->save();
        }
        DB::commit();
    }
}
