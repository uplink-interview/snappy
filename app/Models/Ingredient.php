<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property-read int|null $id
 * @property string $name
 * @property int $stock_quantity
 * @property int $warning_level
 * @property \DateTimeInterface $warning_sent_at
 */
class Ingredient extends Model
{
    use HasFactory;

    protected $hidden = [
        'created_at',
        'updated_at',
        'warning_level',
        'warning_sent_at',
    ];
}
