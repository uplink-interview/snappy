<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property-read int|null $id
 * @property string $name
 * @property ProductIngredient[] $ingredients
 */
class Product extends Model
{
    use HasFactory;

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function ingredients(): HasMany
    {
        return $this->hasMany(ProductIngredient::class);
    }
}
