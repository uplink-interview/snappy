<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Ingredient;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class IngredientService
{
    /**
     * Update stock quantity by the given difference
     * Return true on successful update
     * Return false on negative stock result
     *
     * @param Ingredient $ingredient
     * @param int $quantity_delta
     * @return bool
     */
    public function updateStock(Ingredient $ingredient, int $quantity_delta): bool
    {
        if ($quantity_delta === 0) {
            return false;
        }

        return DB::transaction(static function () use ($ingredient, $quantity_delta): bool {
            DB::table($ingredient->getTable())
                ->where($ingredient->getKeyName(), '=', $ingredient->getKey())
                ->increment('stock_quantity', $quantity_delta)
            ;
            $ingredient->refresh();

            if ($ingredient->stock_quantity < 0) {
                DB::rollBack();
                return false;
            }

            return true;
        });
    }

    /**
     * Atomically update a list of ingredients
     * Fail if any one of them is goes below zero
     * Return all the failed ingredients
     *
     * @param Collection|array<array{Ingredient, int}> $updates
     * @return Collection<Ingredient>
     */
    public function updateStockMulti(iterable $updates): Collection
    {
        return DB::transaction(static function () use ($updates): Collection {
            $failed = [];

            /**
             * @var Ingredient $ingredient
             * @var int $quantity_delta
             */
            foreach ($updates as [$ingredient, $quantity_delta]) {
                DB::table($ingredient->getTable())
                    ->where($ingredient->getKeyName(), '=', $ingredient->getKey())
                    ->increment('stock_quantity', $quantity_delta)
                ;
                $ingredient->refresh();
                if ($ingredient->stock_quantity < 0) {
                    $failed[] = $ingredient;
                }
            }

            if (count($failed) > 0) {
                DB::rollBack();
            }

            return collect($failed);
        });
    }
}
