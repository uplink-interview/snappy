<?php
declare(strict_types=1);

namespace App\Services;

use App\Events\IngredientStockIsLowNotification;
use App\Models\Ingredient;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class OrderService
{
    /**
     * Decrease ingredient quantities for the order
     * Return the order on success
     * Return a list of indexes for the rejected items on failure
     * Stock is left unchanged on failure
     *
     * @param Collection<OrderItem> $orderItems
     * @return Order|array<int>
     */
    public function createOrder(Collection $orderItems, User $user): Order|array
    {
        // make a list of all required ingredients for this order
        /** @var Ingredient[] $allIngredients */
        $allIngredients = [];

        foreach ($orderItems as $item) {
            foreach ($item->product->ingredients as $productIngredient) {
                $allIngredients[$productIngredient->ingredient->id] = $productIngredient->ingredient;
            }
        }

        $result = DB::transaction(fn (): Order|array => $this->processOrder($orderItems, $user, $allIngredients));

        // email any low stock notifications
        $this->emailLowStockNotifications($allIngredients);

        return $result;
    }

    protected function processOrder(Collection $orderItems, User $user, array &$allIngredients): Order|array {
        // lock ingredients
        DB::table('ingredients')
            ->whereIn('id', array_keys($allIngredients))
            ->lockForUpdate()
            ->get()
        ;

        // refresh ingredients, just in case there was a race with another operation that changed stock quantities
        foreach ($allIngredients as $ingredient) {
            $ingredient->refresh();
        }

        $rejected = [];

        // apply order items to ingredient inventory
        foreach ($orderItems as $idx => $item) {
            if ($this->applyOrderItem($item, $allIngredients) === false) {
                $rejected[] = $idx;
            }
        }

        // order rejected
        if (count($rejected) > 0) {
            DB::rollBack();
            return $rejected;
        }

        // order accepted
        // apply stock updates
        foreach ($allIngredients as $ingredient) {
            $ingredient->save();
        }

        // store new order
        $order = new Order();
        $order->createdBy()->associate($user);
        $order->save();

        foreach ($orderItems as $item) {
            $item->order()->associate($order);
            $item->save();
        }

        return $order;
    }

    private function applyOrderItem(OrderItem $item, array &$allIngredients): bool
    {
        $updatedQuantities = [];

        // NOTE: the relevant rows are locked, so the local copies are good references

        // check that all ingredients are available
        foreach ($item->product->ingredients as $productIngredient) {
            $ingredient = $productIngredient->ingredient;
            $requiredQuantity = $item->quantity * $productIngredient->quantity;

            // reject order item if ingredient requirements cannot be satisfied
            if ($ingredient->stock_quantity < $requiredQuantity) {
                return false;
            }

            // remember the new quantity
            $updatedQuantities[$ingredient->id] = $allIngredients[$ingredient->id]->stock_quantity - $requiredQuantity;
        }

        // order item accepted
        // update available quantities
        foreach ($updatedQuantities as $id => $updatedQuantity) {
            $allIngredients[$id]->stock_quantity = $updatedQuantity;
        }

        return true;
    }

    /**
     * @param array<Ingredient> $allIngredients
     * @return void
     */
    protected function emailLowStockNotifications(array $allIngredients): void
    {
        $lowStock = array_filter($allIngredients, static fn (Ingredient $ingredient): bool => $ingredient->stock_quantity <= $ingredient->warning_level);

        IngredientStockIsLowNotification::dispatch($lowStock);
    }
}
