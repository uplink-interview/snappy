<?php

namespace App\Providers;

use App\Http\Responses\ErrorResponseCodeInterface;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Response::macro('error', function (\BackedEnum & ErrorResponseCodeInterface $code, int $status = HttpResponse::HTTP_BAD_REQUEST) {
            return Response::json([
                'error_code' => $code->name,
                'message' => $code->value,
            ], $status);
        });
    }
}
