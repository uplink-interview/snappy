<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderRequest;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Services\OrderService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateOrderRequest $request, OrderService $orderService): JsonResponse
    {
        $orderItems = array_map(static function (array $item) {
            $orderItem = new OrderItem;
            $orderItem->product()->associate(Product::query()->findOrFail($item['id']));
            $orderItem->quantity = $item['quantity'];
            return $orderItem;
        }, $request->validated());

        $order = $orderService->createOrder(collect($orderItems), auth('api')->user());

        if ($order instanceof Order === false) {
            return response()->json([
                'unavailable' => $order,
            ]);
        }

        return response()->json([
            'id' => $order->id,
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Order $order)
    {
        $order->load('items');
        return response()->json($order);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Order $order)
    {
        //
    }
}
