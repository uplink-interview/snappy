<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\UpdateIngredientRequest;
use App\Http\Responses\StockErrorResponseCode;
use App\Models\Ingredient;
use App\Services\IngredientService;
use Illuminate\Http\JsonResponse;

class IngredientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {
        $ingredients = Ingredient::all();

        return response()->json($ingredients);
    }

    /**
     * Display the specified resource.
     */
    public function show(Ingredient $ingredient): JsonResponse
    {
        return response()->json($ingredient->attributesToArray());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateIngredientRequest $request, Ingredient $ingredient, IngredientService $ingredientService): JsonResponse
    {
        $quantity_delta = $request->validated('quantity_delta');

        if ($quantity_delta === 0) {
            return response()->error(StockErrorResponseCode::NO_CHANGE);
        }

        $success = $ingredientService->updateStock($ingredient, $quantity_delta);

        if (!$success) {
            return response()->error(StockErrorResponseCode::INSUFFICIENT_STOCK);
        }

        $ingredient->refresh();
        return response()->json($ingredient);
    }
}
