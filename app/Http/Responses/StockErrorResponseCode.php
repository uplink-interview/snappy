<?php
declare(strict_types=1);

namespace App\Http\Responses;

enum StockErrorResponseCode: string implements ErrorResponseCodeInterface
{
    case INSUFFICIENT_STOCK = 'Insufficient stock';
    case NO_CHANGE = 'No change requested';
}
