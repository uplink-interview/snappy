<?php
declare(strict_types=1);

namespace App\Listeners;

use App\Events\IngredientStockIsLowNotification;
use App\Mail\IngredientLowStockMail;
use App\Models\Ingredient;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendLowStockNotification
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(IngredientStockIsLowNotification $event): void
    {
        $recipient = env('LOW_STOCK_EMAIL_RECIPIENT');
        if (empty($recipient)) {
            return;
        }

        $ingredients = $event->getIngredients();

        if (!is_array($ingredients)) {
            $ingredients = [$ingredients];
        }

        /** @var Ingredient $ingredient */
        foreach ($ingredients as $ingredient) {
            // don't send email if one has been sent in the last 24 hours
            if ($ingredient->warning_sent_at !== null && Carbon::create($ingredient->warning_sent_at)->addDay()->isAfter(Carbon::now())) {
                continue;
            }

            Mail::to($recipient)->send(new IngredientLowStockMail($ingredient));

            $ingredient->warning_sent_at = Carbon::now();
            $ingredient->save();

            $notification = new Notification();
            $notification->ingredient()->associate($ingredient);
            $notification->save();
        }
    }
}
