<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-user {name} {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $user = new User();
        $user->password = Hash::make(random_bytes(10));
        $user->name = $this->argument('name');
        $user->email = $this->argument('email');
        $user->save();

        $this->info("New user ID: {$user->id}");
    }
}
