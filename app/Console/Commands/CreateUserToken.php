<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Tymon\JWTAuth\JWT;

class CreateUserToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-user-token {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Issue a JWT token for a user';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $user_id = $this->argument('user');

        /** @var User $user */
        $user = User::query()->findOrFail($user_id);

        $token = app(JWT::class)->fromUser($user);

        $this->info("Token: $token");
    }
}
