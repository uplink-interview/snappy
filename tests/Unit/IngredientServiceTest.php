<?php
declare(strict_types=1);

namespace Tests\Unit;

use App\Models\Ingredient;
use App\Services\IngredientService;
use Illuminate\Support\Collection;
use Tests\RefreshTheDamnedDatabase;
use Tests\TestCase;

class IngredientServiceTest extends TestCase
{
    use RefreshTheDamnedDatabase;

    private IngredientService $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->refreshTheDamnedDatabase();

        /** @var IngredientService $service */
        $this->service = app(IngredientService::class);
    }

    public function test_update_stock_success(): void
    {
        /** @var Ingredient $ingredient */
        $ingredient = Ingredient::query()->first();

        $success = $this->service->updateStock($ingredient, 1);

        $this->assertTrue($success);

        $ingredient->refresh();
        $this->assertSame(1, $ingredient->stock_quantity);

        /** @var Ingredient[] $allOtherIngredients */
        $allOtherIngredients = Ingredient::query()->whereNot($ingredient->getKeyName(), '=', $ingredient->getKey());
        foreach ($allOtherIngredients as $ingredient) {
            $this->assertSame(0, $ingredient->stock_quantity);
        }
    }

    public function test_update_stock_multi_success(): void
    {
        /** @var Collection|Ingredient[] $ingredients */
        $ingredients = Ingredient::query()->whereIn('name', ['Lettuce', 'Tomato', 'Bacon'])->get();

        $updates = $ingredients->map(static fn (Ingredient $ingredient) => [$ingredient, 1]);
        $failed = $this->service->updateStockMulti($updates);

        $this->assertTrue($failed->isEmpty());
    }

    public function test_update_stock_failure(): void
    {
        /** @var Ingredient $ingredient */
        $ingredient = Ingredient::query()->first();

        $success = $this->service->updateStock($ingredient, -1);

        $this->assertFalse($success);

        /** @var Ingredient[] $allOtherIngredients */
        $allOtherIngredients = Ingredient::all();
        foreach ($allOtherIngredients as $ingredient) {
            $this->assertSame(0, $ingredient->stock_quantity);
        }
    }

    public function test_update_stock_multi_failure(): void
    {
        /** @var Collection|Ingredient[] $ingredients */
        $ingredients = Ingredient::query()->whereIn('name', ['Lettuce', 'Tomato', 'Bacon'])->get();

        $updates = $ingredients->map(static fn (Ingredient $ingredient) => [$ingredient, -1]);
        $failed = $this->service->updateStockMulti($updates);

        $this->assertEquals($ingredients->toArray(), $failed->toArray());

        /** @var Collection|Ingredient[] $ingredients */
        $ingredients = Ingredient::query()->whereIn('name', ['Lettuce', 'Tomato', 'Bacon'])->get();
        foreach ($ingredients as $ingredient) {
            $this->assertGreaterThanOrEqual(0, $ingredient->stock_quantity);
        }
    }
}
