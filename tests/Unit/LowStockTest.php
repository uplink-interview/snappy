<?php
declare(strict_types=1);

namespace Tests\Unit;

use App\Events\IngredientStockIsLowNotification;
use App\Mail\IngredientLowStockMail;
use App\Models\Ingredient;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class LowStockTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        putenv('LOW_STOCK_EMAIL_RECIPIENT=recipient@example.com');

        Mail::fake();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        putenv('LOW_STOCK_EMAIL_RECIPIENT=');
    }

    public function test_low_stock_email(): void
    {
        $ingredient = new Ingredient();
        $ingredient->name = 'Test ingredient';
        $ingredient->stock_quantity = 123;

        IngredientStockIsLowNotification::dispatch($ingredient);
        IngredientStockIsLowNotification::dispatch($ingredient);

        Mail::assertSent(IngredientLowStockMail::class, 1);
    }
}
