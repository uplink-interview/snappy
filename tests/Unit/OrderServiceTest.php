<?php

namespace Tests\Unit;

use App\Models\Ingredient;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;
use App\Services\OrderService;
use Tests\RefreshTheDamnedDatabase;
use Tests\TestCase;

class OrderServiceTest extends TestCase
{
    use RefreshTheDamnedDatabase;

    private OrderService $service;

    private User $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->refreshTheDamnedDatabase();

        /** @var OrderService $service */
        $this->service = app(OrderService::class);

        $this->user = User::factory()->create();
    }

    public function test_all_unavailable(): void
    {
        $items = collect([
            $this->makeOrderItem(Product::query()->find(1), 1),
            $this->makeOrderItem(Product::query()->find(2), 1),
        ]);

        $result = $this->service->createOrder($items, $this->user);

        $this->assertNotInstanceOf(Order::class, $result);
        $this->assertIsArray($result);
        $this->assertSame([0, 1], $result);
    }

    public function test_some_unavailable(): void
    {
        $names = ['Lettuce', 'Bacon', 'Bread'];

        Ingredient::query()
            ->whereIn('name', $names)
            ->update(['stock_quantity' => 1000])
        ;

        $items = collect([
            $this->makeOrderItem(Product::query()->find(1), 1),
            $this->makeOrderItem(Product::query()->find(2), 1),
        ]);

        $result = $this->service->createOrder($items, $this->user);

        $this->assertNotInstanceOf(Order::class, $result);
        $this->assertIsArray($result);
        $this->assertSame([1], $result);
    }

    public function test_valid_order(): void
    {
        $names = ['Lettuce', 'Bacon', 'Bread', 'Tofu', 'Tomato'];

        Ingredient::query()
            ->whereIn('name', $names)
            ->update(['stock_quantity' => 1000])
        ;

        $items = collect([
            $this->makeOrderItem(Product::query()->find(1), 1),
            $this->makeOrderItem(Product::query()->find(2), 1),
        ]);

        $result = $this->service->createOrder($items, $this->user);

        $this->assertInstanceOf(Order::class, $result);
    }

    /**
     * @param Product $product
     * @param int $quantity
     * @return OrderItem
     */
    private function makeOrderItem(Product $product, int $quantity): OrderItem
    {
        $item = new OrderItem();
        $item->product()->associate($product);
        $item->quantity = $quantity;

        return $item;
    }
}
