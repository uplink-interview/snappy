<?php

namespace Tests\Feature;

use App\Http\Responses\StockErrorResponseCode;
use App\Models\Ingredient;
use App\Models\User;
use Database\Seeders\IngredientSeeder;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\RefreshTheDamnedDatabase;
use Tests\TestCase;
use Tymon\JWTAuth\JWT;

class IngredientTest extends TestCase
{
    use RefreshTheDamnedDatabase, WithFaker;

    protected string $token;

    protected function setUp(): void
    {
        parent::setUp();
        $this->refreshTheDamnedDatabase();

        $this->setUpFaker();

        $user = new User();
        $user->password = $this->faker->password;
        $user->name = $this->faker->name;
        $user->email = $this->faker->email;
        $user->save();

        $this->token = app(JWT::class)->fromUser($user);
    }

    /**
     * Test unauthorised access to the ingredient list query endpoint
     */
    public function test_not_authorized(): void
    {
        $response = $this->getJson('/api/ingredient');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Test the ingredient list query endpoint
     */
    public function test_get_ingredients(): void
    {
        $response = $this->withAuth()->getJson('/api/ingredient');

        // check that the response succeeded
        $response->assertStatus(Response::HTTP_OK);

        // check that no session cookie is present
        $response->assertHeaderMissing('Cookies');

        // check that the response matches the seeder
        $body = $response->json();
        $this->assertCount(count(IngredientSeeder::ingredients), $body);

        // check that the ingredient names match the seeder
        $names = array_column($body, 'name');
        $this->assertEquals(IngredientSeeder::ingredients, $names);
    }

    public static function provideIngredients(): iterable
    {
        return Ingredient::all();
    }

    /**
     * Test the individual ingredient query endpoint
     * @dataProvider provideIngredients
     */
    public function test_get_ingredient(Ingredient $ingredient): void
    {
        // check unauthorised access
        $response = $this->getJson('/api/ingredient/' . $ingredient->id);

        $response->assertUnauthorized();

        // check authorised access
        $response = $this->withAuth()->getJson('/api/ingredient/' . $ingredient->id);

        // check that the response succeeded
        $response->assertStatus(Response::HTTP_OK);

        // check that no session cookie is present
        $response->assertHeaderMissing('Cookies');

        // check that the response matches the input
        $body = $response->json();
        $this->assertEquals([
            'id' => $ingredient->id,
            'name' => $ingredient->name,
        ], $body);
    }

    public function test_post_ingredient_unauthorised(): void
    {
        /** @var Ingredient $ingredient */
        $ingredient = Ingredient::query()->first();

        // check unauthorised access
        $response = $this->postJson('/api/ingredient/' . $ingredient->id, [
            'quantity_delta' => 1,
        ]);

        $response->assertUnauthorized();

        $ingredient->refresh();
        $this->assertSame(0, $ingredient->stock_quantity);
    }

    public function test_post_ingredient_authorised(): void
    {
        /** @var Ingredient[] $ingredients */
        $ingredients = Ingredient::all();

        // -- increase quantities to 1000 --
        foreach ($ingredients as $ingredient) {
            $response = $this->withAuth()->postJson('/api/ingredient/' . $ingredient->id, [
                'quantity_delta' => 1000,
            ]);

            $response->assertOk();

            $body = $response->json();
            foreach ([
                'id' => $ingredient->id,
                'name' => $ingredient->name,
                'stock_quantity' => 1000,
            ] as $key => $value) {
                $this->assertSame($value, $body[$key]);
            }
        }

        // -- decrease quantities by 500 --
        foreach ($ingredients as $ingredient) {
            $response = $this->withAuth()->postJson('/api/ingredient/' . $ingredient->id, [
                'quantity_delta' => -500,
            ]);

            $response->assertOk();

            $body = $response->json();
            foreach ([
                         'id' => $ingredient->id,
                         'name' => $ingredient->name,
                         'stock_quantity' => 500,
                     ] as $key => $value) {
                $this->assertSame($value, $body[$key]);
            }
        }

        // -- decrease quantities by 501 (going negative)
        foreach ($ingredients as $ingredient) {
            $response = $this->withAuth()->postJson('/api/ingredient/' . $ingredient->id, [
                'quantity_delta' => -501,
            ]);

            $response->assertBadRequest();

            $body = $response->json();
            $this->assertSame(StockErrorResponseCode::INSUFFICIENT_STOCK->name, $body['error_code']);
            $this->assertSame(StockErrorResponseCode::INSUFFICIENT_STOCK->value, $body['message']);
        }

        // check that quantities are unchanged
        foreach ($ingredients as $ingredient) {
            $ingredient->refresh();
            $this->assertSame(500, $ingredient->stock_quantity);
        }

        // change quantities by 0
        foreach ($ingredients as $ingredient) {
            $response = $this->withAuth()->postJson('/api/ingredient/' . $ingredient->id, [
                'quantity_delta' => 0,
            ]);

            $response->assertBadRequest();

            $body = $response->json();
            $this->assertSame(StockErrorResponseCode::NO_CHANGE->name, $body['error_code']);
            $this->assertSame(StockErrorResponseCode::NO_CHANGE->value, $body['message']);
        }
    }

    public function withAuth(): static
    {
        return $this->withToken($this->token);
    }
}
