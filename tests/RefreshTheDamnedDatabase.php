<?php
declare(strict_types=1);

namespace Tests;

/**
 * Quick and dirty database refresh for tests
 *
 * Reason:
 * - RefreshDatabase uses transactions. That's no bueno when I'm also using transactions.
 * - DatabaseTruncation exploded spectacularly with this beautiful error:
 * ```
 * Class "Doctrine\DBAL\Driver\AbstractMySQLDriver" not found
 * ```
 */
trait RefreshTheDamnedDatabase
{
    public function refreshTheDamnedDatabase(): void
    {
        $this->artisan('db:wipe');
        $this->artisan('migrate --step --seed');
    }
}
