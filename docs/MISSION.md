# Problem Statement

The system should have three main models:
* Product
* Ingredient
* Order

A Burger (Product) can have several ingredients with corresponding stock levels, and the system
keeps track of the stock levels of each ingredient.

When a customer makes an order that includes a Burger, the system needs to update the stock of
the ingredients accordingly.

Additionally, if any of the ingredients' stock levels reach 50%, the system should send an
email message to alert the merchant to purchase more of that ingredient.

The requirements are:

1. to create a DB scheme that will store the information about products, ingredients
   and orders.
2. an endpoint that accepts order details from the request payload, persists the order
   in the database, and updates the stock of the ingredients.
3. The application should also send an email once the level of any of the ingredients
   reaches below 50%, but only a single email should be sent per ingredient per day.
4. Finally, several test cases should be written to assert that the order was correctly
   stored, and the stock was correctly updated.

Please use php 8.2, mysql 5.7 and the latest Laravel. Please send me a link to the repository.
